#######################################################################
def reverser
  yield.split.each{|word| word.reverse!}.join(" ")
end

#######################################################################
def adder(num = 1)
  yield + num
end

#######################################################################
def repeater(n = 1, &prc)
  n.times do
    prc.call
  end
end
